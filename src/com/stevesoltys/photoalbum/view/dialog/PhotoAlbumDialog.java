package com.stevesoltys.photoalbum.view.dialog;

import com.stevesoltys.photoalbum.controller.dialog.DialogController;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

/**
 * A modifiable window dialog.
 *
 * @author Steve Soltys
 * @author Juan Vargas
 */
public class PhotoAlbumDialog extends Stage {

    /**
     * The title of this dialog.
     */
    private final String title;

    /**
     * The resource path.
     */
    private final String resourcePath;

    public PhotoAlbumDialog(String title, String resourcePath) {
        this.title = title;
        this.resourcePath = resourcePath;
    }

    /**
     * Initializes and shows this dialog with the given controller.
     */
    public void init(DialogController controller) {
        setTitle(title);
        setResizable(false);
        setAlwaysOnTop(true);

        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource(resourcePath));
            loader.setController(controller);

            Parent root = loader.load();

            setScene(new Scene(root));

        } catch (IOException e) {
            e.printStackTrace();
            System.exit(1);
        }
    }
}
