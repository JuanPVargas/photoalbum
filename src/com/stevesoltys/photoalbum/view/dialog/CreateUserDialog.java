package com.stevesoltys.photoalbum.view.dialog;

/**
 * @author Steve Soltys
 * @author Juan Vargas
 */
public class CreateUserDialog extends PhotoAlbumDialog {

    public CreateUserDialog() {
        super("Create User", "create_user.fxml");
    }

}
