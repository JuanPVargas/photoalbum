package com.stevesoltys.photoalbum.view.dialog;

/**
 * @author Steve Soltys
 * @author Juan Vargas
 */
public class CaptionPhotoDialog extends PhotoAlbumDialog {

    public CaptionPhotoDialog() {
        super("Caption Photo", "caption_photo.fxml");
    }

}
