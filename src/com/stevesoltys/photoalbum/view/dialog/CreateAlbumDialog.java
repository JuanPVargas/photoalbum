package com.stevesoltys.photoalbum.view.dialog;

/**
 * @author Steve Soltys
 * @author Juan Vargas
 */
public class CreateAlbumDialog extends PhotoAlbumDialog {

    public CreateAlbumDialog() {
        super("Create Album", "create_album.fxml");
    }

}
