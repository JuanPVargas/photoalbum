package com.stevesoltys.photoalbum.view.dialog;

/**
 * @author Steve Soltys
 * @author Juan Vargas
 */
public class RenameAlbumDialog extends PhotoAlbumDialog {

    public RenameAlbumDialog() {
        super("Rename Album", "rename_album.fxml");
    }

}
