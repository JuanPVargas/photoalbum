package com.stevesoltys.photoalbum.view.dialog;

/**
 * @author Steve Soltys
 * @author Juan Vargas
 */
public class MovePhotoDialog extends PhotoAlbumDialog {

    public MovePhotoDialog() {
        super("Move Photo", "move_photo.fxml");
    }

}
