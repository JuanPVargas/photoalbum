package com.stevesoltys.photoalbum.repository;

import com.stevesoltys.photoalbum.model.User;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

/**
 * @author Steve Soltys
 * @author Juan Vargas
 */
public class UserRepository {

    /**
     * The list of {@link User}s.
     */
    private final List<User> users = new LinkedList<>();

    /**
     * Gets a {@link User}, given their username.
     *
     * @param username The username.
     * @return An optional, possibly containing the username.
     */
    public Optional<User> getUser(String username) {
        return users.stream().filter(user -> user.getUsername().equals(username)).findFirst();
    }

    /**
     * Checks to see if the given user has valid credentials.
     *
     * @param user The user.
     * @return A flag indicating whether the user has valid credentials.
     */
    public boolean isValidUser(User user) {
        return users.contains(user);
    }

    /**
     * Checks if a username is taken.
     *
     * @param username The username.
     * @return A flag indicating whether or not the username is taken.
     */
    public boolean isUsernameTaken(String username) {
        return users.stream().anyMatch(user -> user.getUsername().equals(username));
    }

    /**
     * Gets the list of {@link User}s in the repository.
     *
     * @return The list of users.
     */
    public List<User> getUsers() {
        return users;
    }
}
