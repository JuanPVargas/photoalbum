package com.stevesoltys.photoalbum.controller;

import com.stevesoltys.photoalbum.controller.dialog.CaptionPhotoDialogController;
import com.stevesoltys.photoalbum.controller.dialog.CreateAlbumDialogController;
import com.stevesoltys.photoalbum.controller.dialog.MovePhotoDialogController;
import com.stevesoltys.photoalbum.model.Album;
import com.stevesoltys.photoalbum.model.Photo;
import com.stevesoltys.photoalbum.model.User;
import com.stevesoltys.photoalbum.view.dialog.CaptionPhotoDialog;
import com.stevesoltys.photoalbum.view.dialog.CreateAlbumDialog;
import com.stevesoltys.photoalbum.view.dialog.MovePhotoDialog;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.time.Instant;
import java.util.Date;


/**
 * A controller for the Album Management view.
 *
 * @author Steve Soltys
 * @author Juan Vargas
 */
public class AlbumManagementController {

    private final User user;

    private final Album album;

    @FXML
    private ListView<Photo> photoList;

    @FXML
    private Label albumLabel;

    public AlbumManagementController(User user, Album album) {
        this.user = user;
        this.album = album;
    }

    @FXML
    public void initialize() {
        updatePhotoList();

        albumLabel.setText(album.getName());
    }

    private void updatePhotoList() {
        ObservableList<Photo> items = FXCollections.observableArrayList(album.getPhotos());
        photoList.setItems(items);

        photoList.setCellFactory(param -> new ListCell<Photo>() {
            private ImageView imageView = new ImageView();

            @Override
            public void updateItem(Photo photo, boolean empty) {
                super.updateItem(photo, empty);

                if (empty) {
                    setText(null);
                    setGraphic(null);
                } else {
                    imageView.setImage(photo.getThumbnail());

                    setGraphic(imageView);
                    setText(photo.getCaption());
                }
            }
        });
    }

    @FXML
    public void onBackButtonAction(ActionEvent event) throws IOException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("../view/photo_album.fxml"));
        loader.setController(new PhotoAlbumController(user));

        Scene scene = new Scene(loader.load());

        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        stage.setScene(scene);
        stage.show();
    }

    @FXML
    public void onAddButtonAction(ActionEvent event) throws IOException {
        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();

        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Open Image");
        File file = fileChooser.showOpenDialog(stage);

        if(file == null) {
            return;
        }

        FileInputStream fis = new FileInputStream(file);
        Image image = new Image(fis);
        fis.close();

        fis = new FileInputStream(file);
        Image thumbnail = new Image(fis, 100, 100, true, true);
        fis.close();

        Photo photo = new Photo(image, thumbnail, Date.from(Instant.ofEpochMilli(file.lastModified())));
        album.getPhotos().add(photo);

        updatePhotoList();
    }

    @FXML
    public void onDeleteButtonAction(ActionEvent event) {
        Photo selected = photoList.getSelectionModel().getSelectedItem();

        if(selected == null) {
            return;
        }

        album.getPhotos().remove(selected);
        updatePhotoList();
    }

    @FXML
    public void onCaptionButtonAction(ActionEvent event) {
        Photo selected = photoList.getSelectionModel().getSelectedItem();

        if(selected == null) {
            return;
        }

        CaptionPhotoDialog dialog = new CaptionPhotoDialog();
        CaptionPhotoDialogController controller = new CaptionPhotoDialogController(dialog, selected);
        dialog.init(controller);
        dialog.showAndWait();

        updatePhotoList();
    }

    @FXML
    public void onTagButtonAction(ActionEvent event) {

    }

    @FXML
    public void onDisplayButtonAction(ActionEvent event) throws IOException {
        Photo selected = photoList.getSelectionModel().getSelectedItem();

        if(selected == null) {
            return;
        }

        FXMLLoader loader = new FXMLLoader(getClass().getResource("../view/photo_view.fxml"));
        loader.setController(new PhotoViewController(user, album, selected));

        Scene scene = new Scene(loader.load());

        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        stage.setScene(scene);
        stage.show();
    }

    @FXML
    public void onMoveButtonAction(ActionEvent event) {
        Photo selected = photoList.getSelectionModel().getSelectedItem();

        if(selected == null) {
            return;
        }

        MovePhotoDialog dialog = new MovePhotoDialog();
        MovePhotoDialogController controller = new MovePhotoDialogController(dialog, user, album, selected);
        dialog.init(controller);
        dialog.showAndWait();

        updatePhotoList();
    }
}
