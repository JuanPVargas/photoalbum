package com.stevesoltys.photoalbum.controller;

import com.stevesoltys.photoalbum.controller.dialog.CreateUserDialogController;
import com.stevesoltys.photoalbum.io.UserStorageManager;
import com.stevesoltys.photoalbum.model.User;
import com.stevesoltys.photoalbum.repository.UserRepository;
import com.stevesoltys.photoalbum.view.dialog.CreateUserDialog;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.ListView;
import javafx.stage.Stage;

import java.io.IOException;

/**
 * @author Steve Soltys
 * @author Juan Vargas
 */
public class AdminController {

    /**
     * A list of users that are currently in the {@link UserRepository}.
     */
    @FXML
    private ListView<User> userList;

    /**
     * The initialization function for this controller.
     */
    @FXML
    public void initialize() {
        updateUserList();
    }

    /**
     * Populates the user list with the current users in the {@link UserRepository}.
     */
    private void updateUserList() {
        UserRepository repository = UserStorageManager.getRepository();
        userList.setItems(FXCollections.observableArrayList(repository.getUsers()));
    }

    /**
     * Occurs when the user presses the "Logout" button.
     *
     * @param event The action event.
     */
    @FXML
    public void handleLogoutButtonAction(ActionEvent event) throws IOException {
        Parent admin = FXMLLoader.load(getClass().getResource("../view/login.fxml"));
        Scene scene = new Scene(admin);

        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        stage.setScene(scene);
        stage.show();
    }

    /**
     * Occurs when a user presses the "Delete" button.
     *
     * @param event The action event.
     */
    @FXML
    public void handleDeleteButtonAction(ActionEvent event) {
        User user = userList.getSelectionModel().getSelectedItem();

        if(user == null) {
            return;
        }

        UserRepository repository = UserStorageManager.getRepository();
        repository.getUsers().remove(user);

        updateUserList();
    }

    /**
     * Occurs when a user presses the "Create" button.
     *
     * @param event The action event.
     */
    @FXML
    public void handleCreateButtonAction(ActionEvent event) throws IOException {
        CreateUserDialog dialog = new CreateUserDialog();
        CreateUserDialogController controller = new CreateUserDialogController(dialog);
        dialog.init(controller);
        dialog.showAndWait();

        updateUserList();
    }
}
