package com.stevesoltys.photoalbum.controller.dialog;

import com.stevesoltys.photoalbum.model.Photo;
import com.stevesoltys.photoalbum.view.dialog.CaptionPhotoDialog;
import com.stevesoltys.photoalbum.view.dialog.CreateAlbumDialog;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.TextField;

/**
 * A dialog presented when a user clicks the "caption photo" button.
 *
 * @author Steve Soltys
 * @author Juan Vargas
 */
public class CaptionPhotoDialogController extends DialogController {

    /**
     * The photo.
     */
    private final Photo photo;

    /**
     * A text field for the new caption.
     */
    @FXML
    private TextField captionField;

    public CaptionPhotoDialogController(CaptionPhotoDialog dialog, Photo photo) {
        super(dialog);
        this.photo = photo;
    }

    @Override
    public void onDialogButton(ActionEvent event, boolean cancelled) {
        if (cancelled) {
            close();
            return;
        }

        String caption = captionField.getText().trim();

        if (caption.equals("")) {
            Alert alert = new Alert(Alert.AlertType.ERROR, "Please enter a valid caption.");
            alert.initOwner(((Node) event.getSource()).getScene().getWindow());
            alert.showAndWait();
            return;
        }

        photo.setCaption(caption);
        close();
    }
}
