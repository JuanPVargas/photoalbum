package com.stevesoltys.photoalbum.controller.dialog;

import com.stevesoltys.photoalbum.model.Album;
import com.stevesoltys.photoalbum.model.Photo;
import com.stevesoltys.photoalbum.model.User;
import com.stevesoltys.photoalbum.view.dialog.CaptionPhotoDialog;
import com.stevesoltys.photoalbum.view.dialog.MovePhotoDialog;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.TextField;

/**
 * A dialog presented when a user clicks the "move photo" button.
 *
 * @author Steve Soltys
 * @author Juan Vargas
 */
public class MovePhotoDialogController extends DialogController {

    /**
     * The photo.
     */
    private final Photo photo;

    /**
     * The current album.
     */
    private final Album current;

    /**
     * The user.
     */
    private final User user;

    /**
     * A text field for the album name.
     */
    @FXML
    private TextField albumField;

    public MovePhotoDialogController(MovePhotoDialog dialog, User user, Album current, Photo photo) {
        super(dialog);
        this.photo = photo;
        this.current = current;
        this.user = user;
    }

    @Override
    public void onDialogButton(ActionEvent event, boolean cancelled) {
        if (cancelled) {
            close();
            return;
        }

        String name = albumField.getText().trim();
        Album album = new Album(name);

        if (name.equals("")) {
            Alert alert = new Alert(Alert.AlertType.ERROR, "Please enter a valid album name.");
            alert.initOwner(((Node) event.getSource()).getScene().getWindow());
            alert.showAndWait();
            return;
        } else if (!user.getAlbums().contains(album)) {
            Alert alert = new Alert(Alert.AlertType.ERROR, "An album with that name does not exist.");
            alert.initOwner(((Node) event.getSource()).getScene().getWindow());
            alert.showAndWait();
            return;
        }

        int index = user.getAlbums().indexOf(album);
        album = user.getAlbums().get(index);
        album.getPhotos().add(photo);

        current.getPhotos().remove(photo);
        close();
    }
}
