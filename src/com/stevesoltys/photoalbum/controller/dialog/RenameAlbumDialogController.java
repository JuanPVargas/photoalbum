package com.stevesoltys.photoalbum.controller.dialog;

import com.stevesoltys.photoalbum.model.Album;
import com.stevesoltys.photoalbum.model.User;
import com.stevesoltys.photoalbum.view.dialog.CreateAlbumDialog;
import com.stevesoltys.photoalbum.view.dialog.RenameAlbumDialog;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.TextField;

/**
 * @author Steve Soltys
 * @author Juan Vargas
 */
public class RenameAlbumDialogController extends DialogController {

    /**
     * The user.
     */
    private final User user;

    /**
     * The album.
     */
    private final Album album;

    /**
     * A text field for the album's new name.
     */
    @FXML
    private TextField nameField;

    public RenameAlbumDialogController(RenameAlbumDialog dialog, User user, Album album) {
        super(dialog);
        this.user = user;
        this.album = album;
    }

    @Override
    public void onDialogButton(ActionEvent event, boolean cancelled) {
        if (cancelled) {
            close();
            return;
        }

        String name = nameField.getText().trim();

        if (name.equals("")) {
            Alert alert = new Alert(Alert.AlertType.ERROR, "Please enter both a valid album name.");
            alert.initOwner(((Node) event.getSource()).getScene().getWindow());
            alert.showAndWait();
            return;
        } else if (user.getAlbums().contains(new Album(name))) {
            Alert alert = new Alert(Alert.AlertType.ERROR, "That album name has already been taken.");
            alert.initOwner(((Node) event.getSource()).getScene().getWindow());
            alert.showAndWait();
            return;
        }

        album.setName(name);
        close();
    }
}
