package com.stevesoltys.photoalbum.controller.dialog;

import com.stevesoltys.photoalbum.view.dialog.PhotoAlbumDialog;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;

/**
 * A controller for a {@link PhotoAlbumDialog}.
 *
 * @author Steve Soltys
 * @author Juan Vargas
 */
public abstract class DialogController {

    /**
     * The dialog.
     */
    private final PhotoAlbumDialog dialog;

    public DialogController(PhotoAlbumDialog dialog) {
        this.dialog = dialog;
    }

    /**
     * A function called when the "Ok" button is pressed.
     *
     * @param event The action event.
     */
    @FXML
    protected void onOkButtonAction(ActionEvent event) {
        onDialogButton(event, false);
    }

    /**
     * A function called when the "Cancel" button is pressed.
     *
     * @param event The action event.
     */
    @FXML
    protected void onCancelButtonAction(ActionEvent event) {
        onDialogButton(event, true);
    }

    protected void close() {
        dialog.close();
    }

    public abstract void onDialogButton(ActionEvent event, boolean cancelled);

    public PhotoAlbumDialog getDialog() {
        return dialog;
    }
}
