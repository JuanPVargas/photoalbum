package com.stevesoltys.photoalbum.controller.dialog;

import com.stevesoltys.photoalbum.io.UserStorageManager;
import com.stevesoltys.photoalbum.model.User;
import com.stevesoltys.photoalbum.repository.UserRepository;
import com.stevesoltys.photoalbum.view.dialog.CreateUserDialog;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;

/**
 * A dialog presented when the administrator clicks the "create user" button.
 *
 * @author Steve Soltys
 * @author Juan Vargas
 */
public class CreateUserDialogController extends DialogController {

    /**
     * A text field for the new user's username.
     */
    @FXML
    private TextField usernameField;

    /**
     * A password field for the new user's password.
     */
    @FXML
    private PasswordField passwordField;

    public CreateUserDialogController(CreateUserDialog dialog) {
        super(dialog);
    }

    @Override
    public void onDialogButton(ActionEvent event, boolean cancelled) {
        if (cancelled) {
            close();
            return;
        }

        UserRepository repository = UserStorageManager.getRepository();

        String username = usernameField.getText().trim();
        String password = passwordField.getText().trim();

        if (username.equals("") || password.equals("")) {
            Alert alert = new Alert(Alert.AlertType.ERROR, "Please enter both a username and a password.");
            alert.initOwner(((Node) event.getSource()).getScene().getWindow());
            alert.showAndWait();
            return;
        } else if (repository.isUsernameTaken(username)) {
            Alert alert = new Alert(Alert.AlertType.ERROR, "That username has already been taken.");
            alert.initOwner(((Node) event.getSource()).getScene().getWindow());
            alert.showAndWait();
            return;
        }

        User user = new User(username, password);
        repository.getUsers().add(user);
        close();
    }
}
