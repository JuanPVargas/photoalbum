package com.stevesoltys.photoalbum.controller;

import com.stevesoltys.photoalbum.io.UserStorageManager;
import com.stevesoltys.photoalbum.model.User;
import com.stevesoltys.photoalbum.repository.UserRepository;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.io.IOException;

/**
 * @author Steve Soltys
 * @author Juan Vargas
 */
public class LoginController {

    /**
     * The username for the administrator account.
     */
    private static final String ADMINISTRATOR_USERNAME = "admin";

    /**
     * A text field for the user's username.
     */
    @FXML
    public TextField usernameField;

    /**
     * A password field for the user's password.
     */
    @FXML
    public PasswordField passwordField;

    /**
     * Occurs when the user presses the "Login" button.
     *
     * @param event The action event.
     */
    public void handleLoginButtonAction(ActionEvent event) throws IOException {

        String username = usernameField.getText();
        String password = passwordField.getText();

        if(username.equals("") || password.equals("")) {
            Alert alert = new Alert(Alert.AlertType.ERROR, "Please enter a username and password.");
            alert.showAndWait();
            return;
        }

        User user = new User(username, password);

        UserRepository repository = UserStorageManager.getRepository();

        if (!repository.isValidUser(user)) {
            Alert alert = new Alert(Alert.AlertType.ERROR, "Invalid username or password.");
            alert.showAndWait();
            return;
        }

        if(user.getUsername().equals(ADMINISTRATOR_USERNAME)) {

            Parent loader = FXMLLoader.load(getClass().getResource("../view/admin.fxml"));
            Scene scene = new Scene(loader);

            Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
            stage.setScene(scene);
            stage.show();

        } else {

            FXMLLoader loader = new FXMLLoader(getClass().getResource("../view/photo_album.fxml"));
            loader.setController(new PhotoAlbumController(repository.getUser(username).get()));

            Scene scene = new Scene(loader.load());

            Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
            stage.setScene(scene);
            stage.show();

        }
    }
}
