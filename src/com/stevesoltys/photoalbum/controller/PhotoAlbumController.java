package com.stevesoltys.photoalbum.controller;

import com.stevesoltys.photoalbum.controller.dialog.CreateAlbumDialogController;
import com.stevesoltys.photoalbum.controller.dialog.CreateUserDialogController;
import com.stevesoltys.photoalbum.controller.dialog.RenameAlbumDialogController;
import com.stevesoltys.photoalbum.io.UserStorageManager;
import com.stevesoltys.photoalbum.model.Album;
import com.stevesoltys.photoalbum.model.Photo;
import com.stevesoltys.photoalbum.model.User;
import com.stevesoltys.photoalbum.repository.UserRepository;
import com.stevesoltys.photoalbum.view.dialog.CreateAlbumDialog;
import com.stevesoltys.photoalbum.view.dialog.CreateUserDialog;
import com.stevesoltys.photoalbum.view.dialog.RenameAlbumDialog;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 * A controller for the Photo Album view.
 *
 * @author Steve Soltys
 * @author Juan Vargas
 */
public class PhotoAlbumController {

    @FXML
    private Label usernameLabel;

    @FXML
    private ListView<Album> albumList;

    private final User user;

    public PhotoAlbumController(User user) {
        this.user = user;
    }

    @FXML
    public void initialize() {
        updateAlbumList();

        usernameLabel.setText(user.getUsername());
    }

    private void updateAlbumList() {
        albumList.setItems(FXCollections.emptyObservableList());

        List<Album> albums = user.getAlbums();
        albumList.setItems(FXCollections.observableArrayList(albums));
    }

    /**
     * Occurs when the user presses the "Open" button.
     *
     * @param event The action event.
     */
    @FXML
    public void onOpenButtonAction(ActionEvent event) throws IOException {
        Album selected = albumList.getSelectionModel().getSelectedItem();

        if(selected == null) {
            return;
        }

        FXMLLoader loader = new FXMLLoader(getClass().getResource("../view/album_management.fxml"));
        loader.setController(new AlbumManagementController(user, selected));

        Parent parent = loader.load();
        Scene scene = new Scene(parent);

        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        stage.setScene(scene);
        stage.show();
    }

    /**
     * Occurs when the user presses the "Logout" button.
     *
     * @param event The action event.
     */
    @FXML
    public void onLogoutButtonAction(ActionEvent event) throws IOException {
        Parent parent = FXMLLoader.load(getClass().getResource("../view/login.fxml"));
        Scene scene = new Scene(parent);

        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        stage.setScene(scene);
        stage.show();
    }

    /**
     * Occurs when the user presses the "Create" button.
     *
     * @param event The action event.
     */
    @FXML
    public void onCreateButtonAction(ActionEvent event) {
        CreateAlbumDialog dialog = new CreateAlbumDialog();
        CreateAlbumDialogController controller = new CreateAlbumDialogController(dialog, user);
        dialog.init(controller);
        dialog.showAndWait();

        updateAlbumList();
    }

    /**
     * Occurs when the user presses the "Delete" button.
     */
    @FXML
    public void onDeleteButtonAction() {
        Album selected = albumList.getSelectionModel().getSelectedItem();

        if(selected == null) {
            return;
        }

        user.getAlbums().remove(selected);
        updateAlbumList();
    }

    /**
     * Occurs when the user presses the "Rename" button.
     */
    @FXML
    public void onRenameButtonAction() {
        Album selected = albumList.getSelectionModel().getSelectedItem();

        if(selected == null) {
            return;
        }

        RenameAlbumDialog dialog = new RenameAlbumDialog();
        RenameAlbumDialogController controller = new RenameAlbumDialogController(dialog, user, selected);
        dialog.init(controller);
        dialog.showAndWait();

        updateAlbumList();
    }

    @FXML
    public void onInformationButtonAction(ActionEvent event) {
        Album selected = albumList.getSelectionModel().getSelectedItem();

        if (selected == null) {
            return;
        }

        String information = "";

        Date oldest = new Date(), newest = new Date(0);

        for (Photo photo : selected.getPhotos()) {
            if (photo.getDateTaken().before(oldest)) {
                oldest = photo.getDateTaken();
            }

            if (photo.getDateTaken().after(newest)) {
                newest = photo.getDateTaken();
            }
        }

        information += "Oldest: " + oldest + "\n";
        information += "Newest: " + newest + "\n";
        information += "Number of photos: " + selected.getPhotos().size() + "\n";

        Alert alert = new Alert(Alert.AlertType.INFORMATION, information);
        alert.showAndWait();
    }

    @FXML
    public void onSearchButtonAction() {

    }
}
