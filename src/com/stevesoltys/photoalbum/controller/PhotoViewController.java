package com.stevesoltys.photoalbum.controller;

import com.stevesoltys.photoalbum.model.Album;
import com.stevesoltys.photoalbum.model.Photo;
import com.stevesoltys.photoalbum.model.User;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.Map;

/**
 * @author Steve Soltys
 * @author Juan Vargas
 */
public class PhotoViewController {

    private final User user;

    private final Album album;

    private Photo photo;

    @FXML
    private ImageView photoView;

    @FXML
    private Label dateTakenLabel;

    @FXML
    private Label captionLabel;

    @FXML
    private Label tagsLabel;

    public PhotoViewController(User user, Album album, Photo photo) {
        this.user = user;
        this.album = album;
        this.photo = photo;
    }

    @FXML
    public void initialize() {
        updateCurrentPhoto();
    }

    private void updateCurrentPhoto() {
        photoView.setImage(photo.getImage());

        Map<String, String> photoTags = photo.getTags();
        final String[] tagString = {""};
        photoTags.forEach((key, value) -> tagString[0] += (key + ":" + value));
        tagsLabel.setText(tagString[0]);

        captionLabel.setText(photo.getCaption());

        dateTakenLabel.setText(photo.getDateTaken().toString());
    }

    @FXML
    public void onBackButtonAction(ActionEvent event) throws IOException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("../view/album_management.fxml"));
        loader.setController(new AlbumManagementController(user, album));

        Parent parent = loader.load();
        Scene scene = new Scene(parent);

        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        stage.setScene(scene);
        stage.show();
    }

    @FXML
    public void onPreviousButtonAction(ActionEvent event) {
        int index = album.getPhotos().indexOf(photo);
        int nextIndex = (index == 0) ? album.getPhotos().size() - 1 : index - 1;

        this.photo = album.getPhotos().get(nextIndex);
        updateCurrentPhoto();
    }

    @FXML
    public void onNextButtonAction(ActionEvent event) {
        int index = album.getPhotos().indexOf(photo);
        int nextIndex = (index == album.getPhotos().size() - 1) ? 0 : index + 1;

        this.photo = album.getPhotos().get(nextIndex);
        updateCurrentPhoto();
    }

}
