package com.stevesoltys.photoalbum.model;

import java.io.Serializable;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

/**
 *
 *
 * @author Steve Soltys
 * @author Juan Vargas
 */
public class Album implements Serializable {

    /**
     * The name of this album.
     */
    private String name;

    /**
     * The set of photos that are in this album.
     */
    private final List<Photo> photos;

    public Album(String name) {
        this.name = name;
        this.photos = new LinkedList<>();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Album album = (Album) o;

        return name.equals(album.name);

    }

    @Override
    public int hashCode() {
        return name.hashCode();
    }

    @Override
    public String toString() {
        return name;
    }

    /**
     * Gets the set of {@link Photo}s for this album.
     *
     * @return The set of photos.
     */
    public List<Photo> getPhotos() {
        return photos;
    }

    /**
     * Gets the name of this album.
     *
     * @return The name.
     */
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
