package com.stevesoltys.photoalbum.model;

import javafx.embed.swing.SwingFXUtils;
import javafx.scene.image.Image;

import javax.imageio.ImageIO;
import java.io.*;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Steve Soltys
 * @author Juan Vargas
 */
public class Photo implements Serializable {

    private transient Image image;

    private transient Image thumbnail;

    private byte[] imageData;

    private byte[] thumbnailData;

    private String caption;

    private Map<String, String> tags;

    private Date takenDate;

    public Photo(Image image, Image thumbnail, Date takenDate) throws IOException {
        this.image = image;
        this.thumbnail = thumbnail;

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ImageIO.write(SwingFXUtils.fromFXImage(image, null), "jpg", baos);
        this.imageData = baos.toByteArray();
        baos.close();

        baos = new ByteArrayOutputStream();
        ImageIO.write(SwingFXUtils.fromFXImage(thumbnail, null), "jpg", baos);
        this.thumbnailData = baos.toByteArray();
        baos.close();

        this.caption = "";
        this.tags = new HashMap<>();
        this.takenDate = takenDate;
    }

    private void readObject(ObjectInputStream in) throws IOException, ClassNotFoundException {
        in.defaultReadObject();

        ByteArrayInputStream bais = new ByteArrayInputStream(imageData);
        image = SwingFXUtils.toFXImage(ImageIO.read(bais), null);
        bais.close();

        bais = new ByteArrayInputStream(thumbnailData);
        thumbnail = SwingFXUtils.toFXImage(ImageIO.read(bais), null);
        bais.close();
    }

    private void writeObject(ObjectOutputStream out) throws IOException {
        out.defaultWriteObject();
    }

    public Image getThumbnail() {
        return thumbnail;
    }

    public Image getImage() {
        return image;
    }

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    public Map<String, String> getTags() {
        return tags;
    }

    public Date getDateTaken() {
        return takenDate;
    }
}
