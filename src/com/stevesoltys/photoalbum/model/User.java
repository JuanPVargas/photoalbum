package com.stevesoltys.photoalbum.model;

import java.io.Serializable;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

/**
 * Represents a user account.
 *
 * @author Steve Soltys
 * @author Juan Vargas
 */
public class User implements Serializable {

    /**
     * The user's username.
     */
    private final String username;

    /**
     * The user's password.
     */
    private final String password;

    /**
     * The user's set of {@link Album}s.
     */
    private final List<Album> albums;

    public User(String username, String password) {
        this.username = username;
        this.password = password;
        this.albums = new LinkedList<>();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        User user = (User) o;

        return username.equals(user.username) && password.equals(user.password);
    }

    @Override
    public int hashCode() {
        int result = username.hashCode();
        result = 31 * result + password.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return username;
    }

    /**
     * Gets the username for this user.
     *
     * @return The username.
     */
    public String getUsername() {
        return username;
    }

    /**
     * Gets the password for this user.
     *
     * @return The password.
     */
    public String getPassword() {
        return password;
    }

    /**
     * Gets the set of {@link Album}s for this user.
     *
     * @return The albums.
     */
    public List<Album> getAlbums() {
        return albums;
    }
}
