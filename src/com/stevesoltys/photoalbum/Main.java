package com.stevesoltys.photoalbum;
	
import com.stevesoltys.photoalbum.io.UserStorageManager;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.stage.Stage;
import javafx.scene.Scene;

public class Main extends Application {

	private Stage stage;

	@Override
	public void start(Stage stage) throws Exception {
		Parent root = FXMLLoader.load(getClass().getResource("view/login.fxml"));
		stage.setTitle("Photo Album");
		stage.setResizable(false);
		stage.setScene(new Scene(root));
		stage.show();

		this.stage = stage;
	}

	public static void main(String[] args) {
		launch(args);
	}

	public Stage getStage() {
		return stage;
	}
}
