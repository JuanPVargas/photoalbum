package com.stevesoltys.photoalbum.io;


import com.stevesoltys.photoalbum.model.User;
import com.stevesoltys.photoalbum.repository.UserRepository;

import java.io.*;
import java.util.List;
import java.util.Optional;

/**
 * @author Steve Soltys
 * @author Juan Vargas
 */
public class UserStorageManager {

    /**
     * The repository of {@link User}s.
     */
    private static UserRepository userRepository;

    /**
     * Gets the repository of {@link User}s.
     *
     * @return The repository.
     */
    public static UserRepository getRepository() {
        return userRepository;
    }

    /**
     * Load the user repository on startup.
     */
    static {

        try {
            Optional<UserRepository> optional = load();

            if (optional.isPresent()) {
                userRepository = optional.get();
            } else {
                userRepository = new UserRepository();

                User admin = new User("admin", "admin");
                userRepository.getUsers().add(admin);

                User user = new User("test", "test");
                userRepository.getUsers().add(user);
            }


            Runtime.getRuntime().addShutdownHook(new Thread(() -> {
                try {
                    save(userRepository);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }));
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(1);
        }

    }

    /**
     * Private constructor to prevent instantiation.
     */
    private UserStorageManager() {
    }

    /**
     * The location of the {@link UserRepository} storage file.
     */
    private static final String FILE_LOCATION = "user_repository";

    /**
     * Attempts to load a list of {@link User}s from a local file and stores them in a {@link UserRepository}.
     *
     * @return An optional, possibly containing a list of users.
     * @throws Exception If an error occurs while reading the file.
     */
    private static Optional<UserRepository> load() throws Exception {
        File file = new File(FILE_LOCATION);

        if (!file.exists()) {
            return Optional.empty();
        }

        UserRepository repository = new UserRepository();

        ObjectInputStream inputStream = new ObjectInputStream(new FileInputStream(file));

        try {

            List<User> users = (List<User>) inputStream.readObject();
            repository.getUsers().addAll(users);

        } catch (EOFException | ClassCastException ignored) {
            // ignore
        }

        inputStream.close();

        return Optional.of(repository);
    }

    /**
     * Serializes and saves a list of {@link User}s to a file, given a {@link UserRepository}.
     *
     * @param repository The repository.
     * @throws Exception If an error occurs while writing the data to the file.
     */
    private static void save(UserRepository repository) throws Exception {
        File file = new File(FILE_LOCATION);

        ObjectOutputStream outputStream = new ObjectOutputStream(new FileOutputStream(file));
        outputStream.writeObject(repository.getUsers());
        outputStream.close();
    }
}
